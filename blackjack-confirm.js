const readline = require('readline');
const crypto = require('crypto')
const provablyFair = require('./provably-fair')('sha224')
const cardsHelper = require('./cards.helper')
const eightDeck = cardsHelper.createDeck(8);
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

start();

function start(old_seed) {
    old_seed=old_seed||'';
    console.log('\n[CONFIRM PLAY]')
    rl.question(`\nEnter private seed ${old_seed}: `, (secretSeed) => {
        secretSeed = secretSeed||old_seed

        rl.question('Enter public seed: ', (publicSeed) => {
            rl.question('Enter game id: ', (gameId) => {

            var deck = provablyFair.shuffle(eightDeck, secretSeed, provablyFair.hash(publicSeed + gameId))
            console.log('Start deck: ', JSON.stringify(deck.slice(0,52), null, 2));
            console.log('\n---------------------------------')
            start(secretSeed)
            });
            
        });
    })
}
