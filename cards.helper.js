const CARDS = [
    {label: 'Ace of clubs',         value: 1},
    {label: 'Ace of diamonds',      value: 1},
    {label: 'Ace of hearts',        value: 1},
    {label: 'Ace of spades',        value: 1},
    {label: 'Two of clubs',         value: 2},
    {label: 'Two of diamonds',      value: 2},
    {label: 'Two of hearts',        value: 2},
    {label: 'Two of spades',        value: 2},
    {label: 'Three of clubs',       value: 3},
    {label: 'Three of diamonds',    value: 3},
    {label: 'Three of hearts',      value: 3},
    {label: 'Three of spades',      value: 3},
    {label: 'Four of clubs',        value: 4},
    {label: 'Four of diamonds',     value: 4},
    {label: 'Four of hearts',       value: 4},
    {label: 'Four of spades',       value: 4},
    {label: 'Five of clubs',        value: 5},
    {label: 'Five of diamonds',     value: 5},
    {label: 'Five of hearts',       value: 5},
    {label: 'Five of spades',       value: 5},
    {label: 'Six of clubs',         value: 6},
    {label: 'Six of diamonds',      value: 6},
    {label: 'Six of hearts',        value: 6},
    {label: 'Six of spades',        value: 6},
    {label: 'Seven of clubs',       value: 7},
    {label: 'Seven of diamonds',    value: 7},
    {label: 'Seven of hearts',      value: 7},
    {label: 'Seven of spades',      value: 7},
    {label: 'Eight of clubs',       value: 8},
    {label: 'Eight of diamonds',    value: 8},
    {label: 'Eight of hearts',      value: 8},
    {label: 'Eight of spades',      value: 8},
    {label: 'Nine of clubs',        value: 9},
    {label: 'Nine of diamonds',     value: 9},
    {label: 'Nine of hearts',       value: 9},
    {label: 'Nine of spades',       value: 9},
    {label: 'Ten of clubs',         value: 10},
    {label: 'Ten of diamonds',      value: 10},
    {label: 'Ten of hearts',        value: 10},
    {label: 'Ten of spades',        value: 10},
    {label: 'Jack of clubs',        value: 10},
    {label: 'Jack of diamonds',     value: 10},
    {label: 'Jack of hearts',       value: 10},
    {label: 'Jack of spades',       value: 10},
    {label: 'Queen of clubs',       value: 10},
    {label: 'Queen of diamonds',    value: 10},
    {label: 'Queen of hearts',      value: 10},
    {label: 'Queen of spades',      value: 10},
    {label: 'King of clubs',        value: 10},
    {label: 'King of diamonds',     value: 10},
    {label: 'King of hearts',       value: 10},
    {label: 'King of spades',       value: 10} 
];
const LABELED_CARDS = CARDS.map((card)=>{return card.label});
function createDeck(deck_count) {
    var deck = [];
    deck_count = deck_count||8
    for(let i = 0; i<deck_count;i++) {
        deck = deck.concat(Array.from(LABELED_CARDS));
    }
    return deck

}
module.exports = {
    CARDS: CARDS,
    createDeck: createDeck 
};









