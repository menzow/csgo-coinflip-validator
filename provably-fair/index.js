const crypto = require('crypto');


/**
 * Methods:
 *     @param {string} algorithm Length of the generated value.
 *     @returns {ProvablyFair} Resolves with generated string or rejects with error
 */
module.exports = function(algorithm){
    return new ProvablyFair(algorithm)
};


function ProvablyFair(algorithm) {
    this.algorithm = algorithm;
}
ProvablyFair.prototype = {
    algorithm: "sha512",
    randomString : function(len) {
        len = parseInt(len);
        return new Promise((resolve, reject) => {
            crypto.randomBytes(len, (err, buf) => {
                if (err) return reject(err);
                return resolve(buf.toString('hex'));
            });
        })

    },
    hash : function(value, _algorithm) {
        _algorithm = _algorithm || this.algorithm;
        let hash = crypto.createHash(_algorithm);
        return hash.update(value).digest('hex');
    },
    shuffle : function(arr, secretSeed, publicSeed) {
        return shuffle(this.algorithm, arr, secretSeed, publicSeed)
    },
    randomInt : function(size, secretSeed, publicSeed, min, max) {
        return randomInt(this.algorithm, size, secretSeed, publicSeed, min, max);
    }
};


/**
 * Shuffles an array based on the given seeds, using the given HMAC algorithm.
 * @param {string} hmacAlgorithm Algorithm used for computing the HMAC of the given seed pair.
 * @param {[]} arr Array to be shuffled.
 * @param {string|Buffer} secretSeed Secret seed used as HMAC key.
 * @param {string|Buffer} [publicSeed] Public seed used as HMAC data. To prove fairness of random
 * outputs, the hash of `secretSeed` shall be known before revealing `publicSeed`.
 * @param {function} [hmacBufferUIntParser] Function to be used for parsing a UInt from the
 * generated HMAC buffer.
 * @param {function} [fallbackProvider] Function to provide a fallback value in a given range
 * whether no appropriate number can be parsed from the generated HMAC buffer.
 * @returns {[]} A new array instance containing every element of the input.
 */
function shuffle(hmacAlgorithm, arr, secretSeed, publicSeed, hmacBufferUIntParser, fallbackProvider) {
    var result = [].concat(arr);
    for (var i = arr.length - 1; i > 0; i -= 1) {
        // Generate a random integer within [0, i]
        var j = randomInt(hmacAlgorithm, 4, secretSeed, publicSeed + '-' + i, 0, i + 1, hmacBufferUIntParser, fallbackProvider);
        // Exchange `result[i]` and `result[j]`
        var _ref = [result[j], result[i]];
        result[i] = _ref[0];
        result[j] = _ref[1];
    }
    return result;
};
/**
 * Parses a buffer and tries returning the first unsigned integer which fits the given range.
 * @param {Buffer} buf Buffer to be parsed.
 * @param {number} max Maximum value of output (excluded).
 * @param {number} size Size of parsable chunks in bytes. Must be less than or equal to 6.
 * @param {number} [startOffset=0] Where to start reading the buffer.
 * @returns {number} An unsigned integer parsed from the given buffer. If no appropriate number can
 * be parsed, `NaN` is returned.
 */
function parseUIntFromBuffer(buf, max, size, startOffset) {
    startOffset = startOffset || 0;
    // No appropriate number can be parsed
    if (startOffset > buf.length - size) return NaN;
    // Parse next number of the buffer and check whether it fits the given range
    var randomValue = buf.readUIntBE(startOffset, size, true);
    return randomValue < max ? randomValue : parseUIntFromBuffer(buf, max, size, startOffset + 1);
}
/**
 * Generates a random integer based on the given seeds, using the given HMAC algorithm.
 * @param {string} hmacAlgorithm Algorithm used for computing the HMAC of the given seed pair.
 * @param {number} size Size of output in bytes. Must be less than or equal to 6.
 * @param {string|Buffer} secretSeed Secret seed used as HMAC key.
 * @param {string|Buffer} [publicSeed=] Public seed used as HMAC data. To prove fairness of random
 * outputs, the hash of `secretSeed` shall be known before revealing `publicSeed`.
 * @param {number} [min=0] Minimum value of output (included).
 * @param {number} [max=256 ** size] Maximum value of output (excluded).
 * @param {function} [hmacBufferUIntParser=parseUIntFromBuffer] Function to be used for parsing a
 * UInt from the generated HMAC buffer.
 * @param {function} [fallbackProvider=range => Math.floor(range / 2)] Function to provide a
 * fallback value in a given range whether no appropriate number can be parsed from the generated
 * HMAC buffer.
 * @returns {number} An integer within the given range.
 */
function randomInt(hmacAlgorithm, size, secretSeed, publicSeed, min, max, hmacBufferUIntParser, fallbackProvider) {
    publicSeed = publicSeed || '';
    min = min || 0;
    max = max || Math.pow(256, size);
    hmacBufferUIntParser = hmacBufferUIntParser || parseUIntFromBuffer;
    fallbackProvider = fallbackProvider || function(range) {
        return Math.floor(range / 2);
    };
    var maxRange = Math.pow(256, size);
    var range = max - min;
    // Validate parameters
    if (size <= 0 || size > 6) throw new RangeError('Size must be non-negative and less than or equal to 6.');
    if (range <= 0 || range > maxRange) throw new RangeError('Max - min must be non-negative and less than or equal to 256 ** size.');
    // Create HMAC buffer
    var buf = crypto.createHmac(hmacAlgorithm, secretSeed).update(publicSeed).digest();
    // Try generating a random number in the uniform distribution range
    var limit = maxRange - maxRange % range;
    var randomValue = hmacBufferUIntParser(buf, limit, size);
    // Offset generated/fallback number by the given minimum and then fit it inside the given range
    return min + (Number.isNaN(randomValue) ? fallbackProvider(range) : randomValue % range);
}

