const readline = require('readline');
const crypto = require('crypto')
const provablyFair = require('./provably-fair')('sha224')
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

start();

function start(old_seed) {
    old_seed=old_seed||'';
    console.log('\n[CONFIRM PLAY]')
    rl.question(`\nEnter private seed ${old_seed}: `, (secretSeed) => {
        secretSeed = secretSeed||old_seed

        rl.question('Enter public seed: ', (publicSeed) => {
            rl.question('Enter game id: ', (gameId) => {

            var deck = provablyFair.shuffle(['terrorists', 'counter-terrorists'], secretSeed, provablyFair.hash(publicSeed + gameId))
            console.log('Coin flip result: ', deck[0]);
            console.log('\n---------------------------------')
            start(secretSeed)
            });
            
        });
    })
}
